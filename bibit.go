package main

import (
	"fmt"
	"math"
	"os"
	"regexp"
)

var transition = map[string]string{
	"0000": "0000",
	"0001": "1000",
	"0010": "0001",
	"0011": "0010",
	"0100": "0000",
	"0101": "0010",
	"0110": "1011",
	"0111": "1011",
	"1000": "0100",
	"1001": "0101",
	"1010": "0111",
	"1011": "1111",
	"1100": "1101",
	"1101": "1110",
	"1110": "0111",
	"1111": "1111",
}

type triangle struct {
	t bool
	// Type triangle
	// true:  ^
	// false: v
	a byte
	// Low cell: 0
	b byte
	// 1
	c byte
	// 2
	d byte
	// High cell: 3
}

// Отладочный вывод треугольника
func (t *triangle) String() string {
	if t.t {
		return fmt.Sprintf(`/%c %c%c%c\`, t.a, t.b, t.c, t.d)
	} else {
		return fmt.Sprintf(`\%c%c%c %c/`, t.b, t.c, t.d, t.a)
	}
}

func (t *triangle) strTop() string {
	if t.t {
		return fmt.Sprintf("%c", t.a)
	} else {
		return fmt.Sprintf("%c%c%c", t.d, t.c, t.b)
	}
}

func (t *triangle) strBottom() string {
	if t.t {
		return fmt.Sprintf("%c%c%c", t.d, t.c, t.b)
	} else {
		return fmt.Sprintf("%c", t.a)
	}
}

func (t *triangle) translate() (byte, bool) {
	if m, ok := transition[fmt.Sprintf("%c%c%c%c", t.d, t.c, t.b, t.a)]; ok {
		t.a = m[3]
		t.b = m[2]
		t.c = m[1]
		t.d = m[0]
	}
	ok := false
	if t.a == t.b && t.a == t.c && t.a == t.d {
		ok = true
	}
	return t.a, ok
}

func createTriangle(typeTriangle bool, b ...byte) (t triangle) {
	t.t = typeTriangle
	if typeTriangle {
		t.a = b[0]
		t.b = b[1]
		t.c = b[2]
		t.d = b[3]
	} else {
		t.a = b[3]
		t.b = b[0]
		t.c = b[1]
		t.d = b[2]
	}
	return
}

func main() {
	args := os.Args
	errorInput := "Error parameter"
	if len(args) != 2 {
		fmt.Println(errorInput)
		return
	}

	pattern := regexp.MustCompile("^(0|1)+$")
	if !pattern.MatchString(args[1]) {
		fmt.Println(errorInput)
		return
	}

	exp := math.Log2(float64(len(args[1]))) / 2
	if math.Mod(exp, 1) != 0 {
		fmt.Println(errorInput)
		return
	}

	triangles := createTriangles(args[1])

	fmt.Println(createBinaryString(triangles))

	for true {
		var newString string
		createNewString := false
		for !createNewString {
			newString = ""
			createNewString = true
			for _, row := range triangles {
				for _, t := range row {
					c, ok := t.translate()
					if createNewString && ok {
						newString = fmt.Sprintf("%c", c) + newString
					} else {
						createNewString = false
					}
				}
			}
			fmt.Println(createBinaryString(triangles))
		}

		if len(newString) == 1 {
			fmt.Println(newString)
			return
		}

		triangles = createTriangles(newString)
		fmt.Println(createBinaryString(triangles))

		if len(triangles) == 1 {
			for true {
				c, ok := triangles[0][0].translate()
				fmt.Println(triangles[0][0].strBottom() + triangles[0][0].strTop())
				if ok {
					fmt.Printf("%c\n", c)
					return
				}
			}
		}
	}
}

// Create binary string from array triangles
func createBinaryString(triangles [][]*triangle) string {
	var result string
	for _, row := range triangles {
		var sTop, sBottom string
		for _, t := range row {
			sTop = t.strTop() + sTop
			sBottom = t.strBottom() + sBottom
		}
		result = sBottom + sTop + result
	}
	return result
}

// Create array triangles.
// Input parameter binary string.
// Output - triangles array 2d:
func createTriangles(s string) (result [][]*triangle) {
	// Create binary triangle from binary string
	i := 1
	var virtTriangle [][]byte
	for len(s) > 0 {
		line := []byte(s[len(s)-i : len(s)])
		reversLine := make([]byte, len(line))
		for n, c := range line {
			reversLine[len(line)-n-1] = c
		}
		virtTriangle = append(virtTriangle, reversLine)
		s = s[:len(s)-i]
		i += 2
	}

	// Create array triangles from binary triangle
	for i = 0; i < len(virtTriangle); i += 2 {
		len1, len2 := 1, 3
		line1 := virtTriangle[i]
		line2 := virtTriangle[i+1]
		var row []*triangle
		for len(line2) > 0 {
			l1 := make([]byte, len1)
			copy(l1, line1[0:len1])
			l2 := make([]byte, len2)
			copy(l2, line2[0:len2])
			t := createTriangle(len1 == 1, append(l1, l2...)...)
			row = append(row, &t)
			line1 = line1[len1:]
			line2 = line2[len2:]
			len1, len2 = len2, len1
		}
		result = append(result, row)
	}
	return
}
